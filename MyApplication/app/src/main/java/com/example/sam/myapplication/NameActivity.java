package com.example.sam.myapplication;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.renderscript.Script;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;
import java.util.jar.Attributes;

public class NameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);
        final EditText mEdit = (EditText) findViewById(R.id.editText);

        //add new name to table
        final DBHandler db = new DBHandler(this);
        Button addNameBtn = (Button) (findViewById(R.id.addNameBtn));
        addNameBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String edittext = mEdit.getText().toString();//convert data to string
                if (edittext != "") {//if the box is not blank add name
                    db.addName(new Name(edittext));
                    List<Name> names = db.getAllName();
                    String log = "";
                    for (Name name : names) {
                        log = log + name.getName() + "\n"; //get all names in table
                    }
                    Toast.makeText(NameActivity.this, log, Toast.LENGTH_LONG).show();//display all names in toast notification
                }
                else
                    Toast.makeText(NameActivity.this, "name is blank, please try again", Toast.LENGTH_LONG).show();
            }
        });
        //clear all names from table and notify by toast
        Button delButton = (Button) (findViewById(R.id.delButton));
        delButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                db.clearTB();
                Toast.makeText(NameActivity.this, "All names deleted", Toast.LENGTH_LONG).show();
            }
    });
}
}