package com.example.sam.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class WrongActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wrong);
        Button wrongAgainButton = (Button)(findViewById(R.id.wrongAgainButton));
        wrongAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WrongActivity.this,QuestionActivity.class);
                startActivity(intent);
            }
        });
        Button wrongHomeButton = (Button)(findViewById(R.id.wrongHomeButton));
        wrongHomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WrongActivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
    }
    //disable back button
    @Override
    public void onBackPressed() {
    }
}
