package com.example.sam.myapplication;

/**
 * Created by Sam on 28/04/2016.
 */
public class Name {

    private int id;
    private String name;

    public Name()
    {
    }

    public Name(int id,String name)
    {
        this.id=id;
        this.name=name;
    }

    public Name(String name)
    {
        this.name=name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {

        return id;
    }

    public String getName() {
        return name;
    }

}
