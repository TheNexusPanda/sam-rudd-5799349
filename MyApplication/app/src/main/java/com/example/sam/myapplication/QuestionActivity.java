package com.example.sam.myapplication;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class QuestionActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        ImageButton imageButton = (ImageButton)(findViewById(R.id.imageButton));
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionActivity.this,CorrectActivity.class);
                startActivity(intent);
            }
        });
        ImageButton imageButton2 = (ImageButton)(findViewById(R.id.imageButton2));
        imageButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionActivity.this,WrongActivity.class);
                startActivity(intent);
            }
        });
        ImageButton imageButton3 = (ImageButton)(findViewById(R.id.imageButton3));
        imageButton3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionActivity.this,WrongActivity.class);
                startActivity(intent);
            }
        });
        ImageButton imageButton4 = (ImageButton)(findViewById(R.id.imageButton4));
        imageButton4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuestionActivity.this,WrongActivity.class);
                startActivity(intent);
            }
        });
    }
    //disable back button
    @Override
    public void onBackPressed() {
    }
}
